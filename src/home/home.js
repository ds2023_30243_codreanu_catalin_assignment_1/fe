import React from 'react';
import { Button, Container, Jumbotron } from 'reactstrap';
import './login-style.css';
import ChatBox from './ChatBox';

const backgroundStyle = {
  backgroundPosition: 'center',
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  width: '100%',
  height: '1920px',
  backgroundColor: 'beige',
};

const textStyle = { color: 'dark brown' };

class Home extends React.Component {
    state = {
        loggedIn: localStorage.getItem('loggedIn') === 'true', // Check if the user was previously logged in
        error: '', 
    };

  handleLogin = async () => {
    const url = 'http://localhost:8080/login';
  
    const username = document.querySelector('input[type="text"]').value;
    const password = document.querySelector('input[type="password"]').value;
  
    // Construct the data object
    const data = {
      username: username,
      password: password,
    };
  
    try {
      const response = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
        credentials: 'include', // Ensure the session is stored in the browser
      });
  
      const result = await response.json(); // Assuming the response is JSON

      if (response.ok) {
        localStorage.setItem('loggedIn', 'true');
        localStorage.setItem('token', result.token); 
        localStorage.setItem('role', result.role); 
        localStorage.setItem('userId', result.userId);
        localStorage.setItem('username', username)
        this.setState({ loggedIn: true, error: '' }); 
        window.location.reload();

      } else if (response.status === 401) {
        // Invalid credentials, display error message
        this.setState({ error: 'Invalid credentials' });
      } else {
        // Handle other error cases
        console.error('Login failed:', result);
        this.setState({ error: 'An error occurred' });
      }
    } catch (error) {
      // Handle any network errors or exceptions
      console.error('Error:', error);
      this.setState({ error: 'Network error' });
    }
  };  

  render() {
    const { loggedIn, error } = this.state; // Include the error in the state

    return (
      <div>
        <Jumbotron fluid style={backgroundStyle}>
          <Container fluid>
            <h1 className="display-3" style={textStyle}>
              Integrated Medical Monitoring Platform for Home-care assistance
            </h1>
            <p className="lead" style={textStyle}>
              <b>
                Enabling real-time monitoring of patients, remote-assisted care
                services, and smart intake mechanism for prescribed medication.
              </b>
            </p>
            <hr className="my-2" />
            <p style={textStyle}>
              <b>
              Additionally, our smart intake mechanism for prescribed medication simplifies the process of medication management, ensuring that patients adhere to their prescribed regimens accurately and consistently. With automated reminders and intelligent tracking, our platform promotes medication adherence and empowers individuals to take control of their health journey.
              </b>
            </p>
            {!loggedIn && (
              <div>
                <input type="text" placeholder="Username" />
                <input type="password" placeholder="Password" />
                <Button color="primary" onClick={this.handleLogin}>
                  Login
                </Button>
                {error && <div>{error}</div>} {/* Display the error message if it exists */}
              </div>
            )}
            {loggedIn && (
                <ChatBox/>
            )}
          </Container>
        </Jumbotron>
      </div>
    );
  }
}

export default Home;
