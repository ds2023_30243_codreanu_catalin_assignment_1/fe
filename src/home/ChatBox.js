import React, { useState, useEffect } from 'react';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import './chat-box.css';


const ChatBox = () => {
    const [stompClient, setStompClient] = useState(null);
    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState("");
    const [isTyping, setIsTyping] = useState(false);
    const userId = localStorage.getItem('userId');
    const username = localStorage.getItem('username');

    // Connect to WebSocket
    useEffect(() => {
        const socket = new SockJS('http://localhost:8083/chat'); // Replace with your server URL
        const stompClient = Stomp.over(socket);
        stompClient.connect({}, () => {
            setStompClient(stompClient);
            console.log("Connect");
            stompClient.subscribe('/topic/messages', (message) => {
                const receivedMessage = JSON.parse(message.body);
                
                // Handle typing notifications
                if (receivedMessage.content === "TYPING") {
                    setIsTyping(true);
                } else if (receivedMessage.content === "NOTTYPING") {
                    setIsTyping(false);
                } else {
                    setIsTyping(false);
                    setMessages(prevMessages => [...prevMessages, receivedMessage]);
                }
            });
        });

        return () => {
            if (stompClient !== null) {
                stompClient.disconnect();
            }
        };
    }, []);
    
    const handleInputChange = (e) => {
        setMessage(e.target.value);

        if (!isTyping) {
            stompClient.send("/app/sendMessage", {}, JSON.stringify({ senderId: userId, content: "TYPING" }));
            setIsTyping(true);
        }

        if (e.target.value === "") {
            stompClient.send("/app/sendMessage", {}, JSON.stringify({ senderId: userId, content: "NOTTYPING" }));
            setIsTyping(false);
        }
    };

    // Send a message
    const sendMessage = () => {
        setIsTyping(false);
        if (stompClient && message) {
            console.log(message);
            const chatMessage = { senderId: userId, content: username + ": " + message };
            stompClient.send("/app/sendMessage", {}, JSON.stringify(chatMessage));
            setMessage("");
        }
    };

    return (
        <div className='chat-container'>
            <div className="chat-box">
            {messages.map((msg, index) => (
        <React.Fragment key={index}>
            <div className={"incoming"}>
                {msg.content}
            </div>
            <br/>
        </React.Fragment>
    ))}
                            {isTyping && <div className="typing-animation">User is typing...</div>}

            </div>
            <input
                className="message-input"
                type="text"
                value={message}
                onChange={handleInputChange}
                placeholder="Type a message..."
            />
            <button id="chatButton" onClick={sendMessage}>Send</button>
        </div>
    );
};

export default ChatBox;
