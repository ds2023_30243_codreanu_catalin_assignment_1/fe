// AuthProvider.js
import React, { createContext, useContext, useState } from 'react';

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [token, setToken] = useState(localStorage.getItem('token') || null);
  const [userId, setUserId] = useState(localStorage.getItem('userId') || null);
  const [role, setRole] = useState(localStorage.getItem('role') || null);
  const [username, setUsername] = useState(localStorage.getItem('username') || null);
  const [loggedIn, setLoggedIn] = useState(localStorage.getItem('loggedIn') === 'true' || false); // Initialize loggedIn from localStorage
  const url = 'http://localhost:8080/login'

  const handleLogout = () => {
    fetch(url, {
      method: 'DELETE',
      credentials: 'include', // Include credentials such as cookies in the request
    })
      .then(response => {
        if (response.ok) {
          // Handle the successful response
          console.log('Logout successful');
          // Add any necessary code for handling the successful logout
        } else {
          // Handle the error response
          console.error('Logout failed');
          // Add any necessary code for handling the failed logout
        }
      })
      .catch(error => {
        // Handle any network errors
        console.error('Network error', error);
        // Add any necessary code for handling network errors
      });

    setRole(null);
    setUserId(null);
    setToken(null);
    setLoggedIn(false); // Update loggedIn to false on logout
    setUsername(null);

    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('userId');
    localStorage.setItem('loggedIn', 'false'); // Save the updated loggedIn status to localStorage
  };

  return (
    <AuthContext.Provider value={{ setToken, userId, setUserId, token, role, handleLogout, loggedIn, username }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error('useAuth must be used within an AuthProvider');
  }
  return context;
};

