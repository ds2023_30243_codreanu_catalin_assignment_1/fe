import React from 'react';
import Table from '../../commons/tables/table';
import * as API_USERS from '../api/person-api';

const buttonStyle = {
  fontSize: '0.8em',
  padding: '0.3em 0.5em',
  margin: '0 0.3em',
};

const columns = [
  {
    Header: 'Id',
    accessor: 'id',
  },
  {
    Header: 'Name',
    accessor: 'name',
  },
  {
    Header: 'Address',
    accessor: 'address',
  },
  {
    Header: 'Age',
    accessor: 'age',
  },
  {
    Header: 'Username',
    accessor: 'username',
  },
  {
    Header: 'Actions',
    Cell: ({ row }) => (
      <div>
        <button style={buttonStyle} onClick={() => handleUpdate(row.id)}>
          Update
        </button>
        <button style={buttonStyle} onClick={() => handleDelete(row.id)}>
          Delete
        </button>
      </div>
    ),
  },
];

const filters = [
  {
    accessor: 'name',
  },
];

const handleDelete = (id) => {
  API_USERS.deletePerson(id, (result, status, error) => {
    if (result !== null && status === 200) {
      console.log('Successfully deleted person with id: ' + id);
      // Call the function to reload the table data if needed
    } else {
      console.error('Error deleting person: ', error);
    }
  });
};

const handleUpdate = (id) => {
  // Assuming there is a data structure or function to retrieve the entry details based on the id
  const entryDetails = getEntryDetailsById(id);

  // Create a popup container
  const popupContainer = document.createElement('div');
  popupContainer.className = 'popup-container';

  const nameInput = document.createElement('input');
  nameInput.type = 'text';
  nameInput.placeholder = 'Name';
  nameInput.value = entryDetails.name;

  const addressInput = document.createElement('input');
  addressInput.type = 'text';
  addressInput.placeholder = 'Address';
  addressInput.value = entryDetails.address;

  const ageInput = document.createElement('input');
  ageInput.type = 'text';
  ageInput.placeholder = 'Age';
  ageInput.value = entryDetails.age;

  const usernameInput = document.createElement('input');
  usernameInput.type = 'text';
  usernameInput.placeholder = 'Username';
  usernameInput.value = entryDetails.username;
  
  const passwordInput = document.createElement('input');
  passwordInput.type = 'text';
  passwordInput.placeholder = 'Password';
  passwordInput.value = entryDetails.password;

  // Create a button to perform the update
  const updateButton = document.createElement('button');
  updateButton.textContent = 'Update';
  updateButton.addEventListener('click', () => {
    // Implement the logic for updating the entry with the provided id based on the input values
    const updatedEntry = {
      name: nameInput.value || -1,
      age: ageInput.value !== null ? ageInput.value : -1,
      username: usernameInput.value || -1,
      address: addressInput.value || -1,
      password: passwordInput.value || -1
    };

    // Call the function to update the entry with the provided id
    updateEntryById(id, updatedEntry);

    // Close the popup
    document.body.removeChild(popupContainer);
  });

  // Append input fields and button to the popup container
  popupContainer.appendChild(nameInput);
  popupContainer.appendChild(addressInput);
  popupContainer.appendChild(ageInput);
  popupContainer.appendChild(usernameInput);
  popupContainer.appendChild(passwordInput);
  popupContainer.appendChild(updateButton);

  // Append the popup container to the body
  document.body.appendChild(popupContainer);

  console.log('Updating item with id: ', id);
};

// Example function to retrieve entry details by id (replace with your own implementation)
const getEntryDetailsById = (id) => {
  // Replace this with your own logic to retrieve entry details based on the id
  return {
    name: '',
    address: '',
    age: null,
    username: '',
    password: '',
  };
};

// Example function to update entry by id (replace with your own implementation)
const updateEntryById = (id, updatedEntry) => {
  API_USERS.updatePerson(id, updatedEntry, (result, status, error) => {
    if (result !== null && status === 200) {
      console.log('Successfully updated person with id: ' + id);
      // Call the function to reload the table data if needed
    } else {
      console.error('Error deleting person: ', error);
    }
  });
};

class PersonTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tableData: this.props.tableData,
    };
  }

  render() {
    return (
      <Table
        data={this.state.tableData}
        columns={columns}
        search={filters}
        pageSize={5}
      />
    );
  }
}

export default PersonTable;
