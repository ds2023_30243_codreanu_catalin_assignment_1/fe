import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    person: '/person'
};

function getPersons(callback) {
    const token = localStorage.getItem('token');
    console.log('token:', token); // Check the value of token

    let url = HOST.backend_api + endpoint.person;
    console.log('url:', url); // Check the constructed URL

    fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        credentials: 'include'
    })
        .then(response => {
            console.log('response:', response); // Check the response
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            console.log('data:', data); // Check the received data
            callback(data);
        })
        .catch(error => console.error('Error fetching data: ', error));
}

function getPersonById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.person + params.id, {
       method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPerson(user, callback) {
    const token = localStorage.getItem('token');
    console.log('token:', token); // Check the value of token

    const url = HOST.backend_api + endpoint.person;

    fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        credentials: 'include',
        body: JSON.stringify(user)
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => callback(data, 200, null)) // Assuming a successful response here
        .catch(error => console.error('Error posting data: ', error));
}

function deletePerson(id, callback) {
    const token = localStorage.getItem('token');
    console.log('token:', token); // Check the value of token

    const url = HOST.backend_api + endpoint.person + `/${id}`;

    fetch(url, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`
        },
        credentials: 'include'
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            window.location.reload();
            
            return response.json();
        })
        .then(data => callback(data, 200, null)) // Assuming a successful response here
        .catch(error => console.error('Error deleting data: ', error));
}

function updatePerson(id, updatedPerson, callback) {
    const token = localStorage.getItem('token');
    console.log('token:', token); // Check the value of token

    const url = HOST.backend_api + endpoint.person + `/${id}`;

    fetch(url, {
        method: 'PUT', // Use the PUT method for updates
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        credentials: 'include',
        body: JSON.stringify(updatedPerson)
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            window.location.reload();
            
            return response.json();
        })
        .then(data => callback(data, 200, null)) // Assuming a successful response here
        .catch(error => console.error('Error updating data: ', error));
}

export {
    getPersons,
    getPersonById,
    postPerson,
    deletePerson,
    updatePerson // Add the new function to export
};