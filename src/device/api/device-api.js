import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    device: '/device'
};

function getDevice(callback) {
    const userId = localStorage.getItem('userId');
    const token = localStorage.getItem('token');
    console.log('userId:', userId); // Check the constructed URL

    //let url = HOST.backend_api + endpoint.device;
    let url = `${HOST.backend_device_api}${endpoint.device}/user/${userId}`;
    console.log('url:', url); // Check the constructed URL

    fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        credentials: 'include'
    })
        .then(response => {
            console.log('response:', response); // Check the response
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            console.log('data:', data); // Check the received data
            callback(data);
        })
        .catch(error => console.error('Error fetching data: ', error));
}

function getDeviceById(params, callback){
    const token = localStorage.getItem('token');

    let request = new Request(HOST.backend_device_api + endpoint.device + params.id, {
       method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postDevice(device, callback) {
    const token = localStorage.getItem('token');
    const url = HOST.backend_device_api + endpoint.device;

    fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        credentials: 'include',
        body: JSON.stringify(device)
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => callback(data, 200, null)) // Assuming a successful response here
        .catch(error => console.error('Error posting data: ', error));
}

function deleteDevice(id, callback) {
    const token = localStorage.getItem('token');
    const url = HOST.backend_device_api + endpoint.device + `/${id}`;

    console.log('url:', url); // Check the constructed URL

    fetch(url, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`
        },
        credentials: 'include'
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            window.location.reload();
            
            return response.json();
        })
        .then(data => callback(data, 200, null)) // Assuming a successful response here
        .catch(error => console.error('Error deleting data: ', error));
}

function updateDevice(id, updatedDevice, callback) {
    const token = localStorage.getItem('token');
    console.log('token:', token); // Check the value of token
    console.log('updated device:', updatedDevice); 

    const url = HOST.backend_device_api + endpoint.device + `/${id}`;

    fetch(url, {
        method: 'PUT', // Use the PUT method for updates
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        credentials: 'include',
        body: JSON.stringify(updatedDevice)
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            window.location.reload();
            
            return response.json();
        })
        .then(data => callback(data, 200, null)) // Assuming a successful response here
        .catch(error => console.error('Error updating data: ', error));
}

export {
    getDevice,
    getDeviceById,
    postDevice,
    deleteDevice,
    updateDevice // Add the new function to export
};