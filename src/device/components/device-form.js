import React from 'react';
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/device-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';



class DeviceForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: true,

            formControls: {
                description: {
                    value: '',
                    placeholder: 'Description...',
                    valid: false,
                    touched: false,
                },
                maximumHourlyEnergyConsumption: {
                    value: '',
                    placeholder: 'Maximum Hourly Energy Consumption...',
                    valid: false,
                    touched: false,
                },
                address: {
                    value: '',
                    placeholder: 'Address...',
                    valid: false,
                    touched: false,
                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        // for (let updatedFormElementName in updatedControls) {
        //     formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        // }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registerDevice(device) {
        const callback = (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted device with id: " + result);
                this.reloadHandler();
            } else {
                this.setState({
                    errorStatus: status,
                    error: error
                });
            }
        };
    
        API_USERS.postDevice(device, callback);
    }

    handleSubmit() {
        const userId = localStorage.getItem('userId');

        let device = {
            description: this.state.formControls.description.value,
            maximumHourlyEnergyConsumption: this.state.formControls.maximumHourlyEnergyConsumption.value,
            address: this.state.formControls.address.value,
            userId: userId
        };

        console.log(device);
        this.registerDevice(device);
    }

    render() {
        return (
            <div>

                <FormGroup id='description'>
                    <Label for='descriptionField'> Description: </Label>
                    <Input name='description' id='descriptionField' placeholder={this.state.formControls.description.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.description.value}
                           touched={this.state.formControls.description.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='maximumHourlyEnergyConsumption'>
                    <Label for='maximumHourlyEnergyConsumptionField'> Maximum Hourly Energy Consumption: </Label>
                    <Input name='maximumHourlyEnergyConsumption' id='maximumHourlyEnergyConsumptionField' placeholder={this.state.formControls.maximumHourlyEnergyConsumption.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.maximumHourlyEnergyConsumption.value}
                           touched={this.state.formControls.maximumHourlyEnergyConsumption.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='address'> Address: </Label>
                    <Input name='address' id='address' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           required
                    />
                </FormGroup>

                <Row>
                    <Col sm={{ size: '4', offset: 8 }}>
                        <Button
                            type={'submit'}
                            disabled={!this.state.formIsValid}
                            onClick={this.handleSubmit}
                        >
                            Submit
                        </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default DeviceForm;
