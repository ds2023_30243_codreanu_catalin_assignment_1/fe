import React from 'react';
import Table from '../../commons/tables/table';
import * as API_DEVICE from '../api/device-api';

const buttonStyle = {
  fontSize: '0.8em',
  padding: '0.3em 0.5em',
  margin: '0 0.3em',
};

const columns = [
  {
    Header: 'Id',
    accessor: 'id',
  },
  {
    Header: 'Description',
    accessor: 'description',
  },
  {
    Header: 'Address',
    accessor: 'address',
  },
  {
    Header: 'Maximum Hourly Energy Consumption',
    accessor: 'getMaximumHourlyEnergyConsumption',
  },
  {
    Header: 'Actions',
    Cell: ({ row }) => (
      <div>
        <button style={buttonStyle} onClick={() => handleUpdate(row.id)}>
          Update
        </button>
        <button style={buttonStyle} onClick={() => handleDelete(row.id)}>
          Delete
        </button>
      </div>
    ),
  },
];

const filters = [
  {
    accessor: 'name',
  },
];

const handleDelete = (id) => {
  API_DEVICE.deleteDevice(id, (result, status, error) => {
    if (result !== null && status === 200) {
      console.log('Successfully deleted device with id: ' + id);
      // Call the function to reload the table data if needed
    } else {
      console.error('Error deleting device: ', error);
    }
  });
};

const handleUpdate = (id) => {
  // Assuming there is a data structure or function to retrieve the entry details based on the id
  const entryDetails = getEntryDetailsById(id);

  // Create a popup container
  const popupContainer = document.createElement('div');
  popupContainer.className = 'popup-container';

  const descriptionInput = document.createElement('input');
  descriptionInput.type = 'text';
  descriptionInput.placeholder = 'Description';
  descriptionInput.value = entryDetails.description;

  const addressInput = document.createElement('input');
  addressInput.type = 'text';
  addressInput.placeholder = 'Address';
  addressInput.value = entryDetails.address;

  const maximumHourlyEnergyConsumptionFieldInput = document.createElement('input');
  maximumHourlyEnergyConsumptionFieldInput.type = 'text';
  maximumHourlyEnergyConsumptionFieldInput.placeholder = 'Maximum Hourly Energy Consumption';
  maximumHourlyEnergyConsumptionFieldInput.value = entryDetails.maximumHourlyEnergyConsumptionFieldInput;

  // Create a button to perform the update
  const updateButton = document.createElement('button');
  updateButton.textContent = 'Update';
  updateButton.addEventListener('click', () => {
    // Implement the logic for updating the entry with the provided id based on the input values
    const updatedEntry = {
      id: id,
      description: descriptionInput.value || -1,
      address: addressInput.value || -1,
      maximumHourlyEnergyConsumption: maximumHourlyEnergyConsumptionFieldInput.value || -1,
      userId: '594d1485-be17-481e-89bd-9b4a6fdcf800'
    };

    // Call the function to update the entry with the provided id
    updateEntryById(id, updatedEntry);

    // Close the popup
    document.body.removeChild(popupContainer);
  });

  // Append input fields and button to the popup container
  popupContainer.appendChild(descriptionInput);
  popupContainer.appendChild(addressInput);
  popupContainer.appendChild(maximumHourlyEnergyConsumptionFieldInput);
  popupContainer.appendChild(updateButton);

  // Append the popup container to the body
  document.body.appendChild(popupContainer);

  console.log('Updating item with id: ', id);
};

// Example function to retrieve entry details by id (replace with your own implementation)
const getEntryDetailsById = (id) => {
  // Replace this with your own logic to retrieve entry details based on the id
  return {
    description: '',
    address: '',
    maximumHourlyEnergyConsumptionFieldInput: null
  };
};

// Example function to update entry by id (replace with your own implementation)
const updateEntryById = (id, updatedEntry) => {
  API_DEVICE.updateDevice(id, updatedEntry, (result, status, error) => {
    if (result !== null && status === 200) {
      console.log('Successfully updated device with id: ' + id);
      // Call the function to reload the table data if needed
    } else {
      console.error('Error deleting device: ', error);
    }
  });
};

class DeviceTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tableData: this.props.tableData,
    };
  }

  render() {
    return (
      <Table
        data={this.state.tableData}
        columns={columns}
        search={filters}
        pageSize={5}
      />
    );
  }
}

export default DeviceTable;
