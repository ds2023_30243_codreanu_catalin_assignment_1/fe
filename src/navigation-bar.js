import React from 'react';
import logo from './commons/images/icon.png';
import { useAuth } from './commons/AuthProvider';
import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const NavigationBar = () => {
    const { role, handleLogout, loggedIn } = useAuth(); // Destructure the role and handleLogout from useAuth
    const isAdmin = role === 'Admin';

    return (
        <div>
            <Navbar color="dark" light expand="md">
                <NavbarBrand href="/">
                    <img src={logo} width={"50"} height={"35"} alt="logo" />
                </NavbarBrand>
                <Nav className="mr-auto" navbar>
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle style={textStyle} nav caret>
                            Menu
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem>
                                <NavLink href="/">Home</NavLink>
                            </DropdownItem>
                            {isAdmin && (
                                <DropdownItem>
                                    <NavLink href="/person">Person</NavLink>
                                </DropdownItem>
                            )}
                            {loggedIn && (
                            <DropdownItem>
                                <NavLink href="/device">Device</NavLink>
                            </DropdownItem>
                            )}
                            {loggedIn && (
                                <DropdownItem>
                                    <NavLink onClick={handleLogout} href="/">
                                        Log Out
                                    </NavLink>
                                </DropdownItem>
                            )}
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </Nav>
            </Navbar>
        </div>
    );
};

export default NavigationBar;
